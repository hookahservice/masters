$(window).on('load', function () {
    setTimeout(() => hideSectionVideo(), 4500);
});

/**
 * Функция скрывает и показывает элементы на странице
 */
function hideSectionVideo(){
    document.getElementById('videoSection').style.display = 'none';
    document.getElementById('header').style.display = 'block';
    document.getElementById('mainBlock').style.display = 'block';
    document.getElementById('footer').style.display = 'block';
}