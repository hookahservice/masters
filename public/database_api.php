<?php

chdir('../');
include 'src/Core/Controllers/ObjectController.php';

$type = $_POST['typeRequest'];


switch ($type) {
    case 'getDropDownTypeTabak':
        {
            $tablename = 'tabaktype';
            $requestType = 'select';
            $res = getData($tablename, $requestType);
            echo json_encode($res);
        }
        break;
    case 'getFilterInfo':
        {
            $tablename = 'hookahtype';
            $requestType = 'select';
            $res = getData($tablename, $requestType);
            echo json_encode($res);
        }
        break;
}

/**
 * @param $tablename
 * @param $requestType
 * @return array
 */
function getData($tablename, $requestType)
{
    $obj = new ObjectController();
    $result = $obj->SQLProvider($tablename, $requestType);
    return $result;
}

function result($result)
{
    echo json_encode($result);
}
