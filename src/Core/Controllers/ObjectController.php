<?php

/**
 * LOCAL_SERVER_IP - Айпи сервера
 * LOCAL_SERVER_USER - Юзер
 * LOCAL_SERVER_PASSWORD - Пароль
 * LOCAL_DATABASE_NAME - название базы даных
 */

include 'control_configs/db_config.php';
/**
 * Class ObjectController
 */
class ObjectController
{
    /**
     * Получение линка для подключения
     * @return false|mysqli
     */
    function get_Link()
    {
        $link = mysqli_connect(LOCAL_SERVER_IP, LOCAL_SERVER_USER, LOCAL_SERVER_PASSWORD, LOCAL_DATABASE_NAME);
        return $link;
    }

    /**
     * Подключение
     * @param $link
     * @return mixed
     */
    function get_connect($link)
    {
        if (mysqli_connect_errno()) {
            echo 'Ошибка к подключению к Базе Даных (' . mysqli_connect_errno() . '): ' . mysqli_connect_error();
            exit();
        }
        return $link;
    }

    /**
     *
     * SQLProvider - служит для определения запроса
     * @param $tablename
     * @param $requestType
     * @return array
     */
    public function SQLProvider($tablename, $requestType)
    {
        $link = $this->get_Link();
        $connection = $this->get_connect($link);
        switch ($requestType) {
            case 'select':
                {
                    $request = $this->selectFromBD($link, $tablename);
                    return $request;
                }
                break;
            case 'insert':
                {
                    //TODO
                }
                break;
        }
    }

    /**
     * СЕЛЕКТ TODO
     * @param $link
     * @return array
     */
    function selectFromBD($link, $tablename)
    {
        $sql = "SELECT * FROM " . $tablename . " ";
        $result = mysqli_query($link, $sql);
        $data = mysqli_fetch_all($result, MYSQLI_ASSOC);
        mysqli_close($link);
        return $data;
        //SELECT `ID`, `Name`, `Type`, `Brand` FROM `hookahtype` WHERE 1
    }

}